global.appConfig = {
    envName: 'PPR Environment',
    baseUrl: 'https://knewless.tk.api/',
    swaggerUrl: 'https://knewless.tk/api/swagger-ui/index.html',

    users: {
        Teststudent:{
            email: 'dimusha.mihail@gmail.com',
            password: '123456qw',
        },
        Testauthor:{
            email: 'dimusha.mihail+1@gmail.com',
            password: '956874lv',
        },
    },
};