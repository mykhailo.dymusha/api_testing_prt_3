import { ApiRequest } from "../request";
let baseUrl: string = 'https://knewless.tk/api/'

export class ArticleCommentController{
    async saveComment(accessToken:string, articleId :string, commentText :string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method('POST')
            .url(`aritcle_comment`)
            .bearerToken(accessToken)
            .body({"articleId": articleId, "id": articleId, "text" : commentText})
            .send();
        return response;
    }

    async getCommentsByArticle(articleId :string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method('GET')
            .url(`aritcle_comment/of/${articleId}`)
            .send();
        return response;
    }
}