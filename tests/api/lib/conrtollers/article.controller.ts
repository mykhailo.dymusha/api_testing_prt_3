import { ApiRequest } from "../request";
let baseUrl: string = 'https://knewless.tk/api/'

export class ArticleController{
    async saveArticle(accessToken: string, articleBody: object) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method('POST')
            .url(`aritcle`)
            .bearerToken(accessToken)
            .body(articleBody)
            .send();
        return response;
    }

    async getArticles() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method('GET')
            .url(`aritcle/author`)
            .body({})
            .send();
        return response;
    }

    async getArticleById(articleId : string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method('GET')
            .url(`aritcle/${articleId}`)
            .body({})
            .send();
        return response;
    }
}
