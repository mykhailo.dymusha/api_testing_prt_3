import { ApiRequest } from "../request";
let baseUrl: string = 'https://knewless.tk/api/'

export class AuthController {
    async authenticateUser(userEmail :string, userPassword :string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method('POST')
            .url(`auth/login`)
            .body({"email": userEmail , "password": userPassword})
            .send();
        return response;
    }
}

