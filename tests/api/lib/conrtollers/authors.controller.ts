import { ApiRequest } from "../request";
let baseUrl: string = 'https://knewless.tk/api/'

export class AuthorController {
    async getAuthorSettings(accessToken :string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method('GET')
            .url(`/author`)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async setAuthorSettings(accessToken : string, authorsSettings : object) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method('POST')
            .url(`/author`)
            .bearerToken(accessToken)
            .body(authorsSettings)
            .send();
        return response;
    }

    async getPublicAuthor(authorId :string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method('GET')
            .url(`/author/overview/${authorId}`)
            .send();
        return response;
    }
}
