import {expect} from "chai";
import { describe } from "mocha";
import {checkResponseBodyMessage, checkResponseTime, checkStatusCode} from "../../../helpers/functionsForChecking.helper";
import { AuthController } from "tests/api/lib/conrtollers/auth.controller";
import { AuthorController } from "tests/api/lib/conrtollers/authors.controller";
import { UserController } from "tests/api/lib/conrtollers/user.controller";
import { ArticleController } from "tests/api/lib/conrtollers/article.controller";
import { ArticleCommentController } from "tests/api/lib/conrtollers/article-comment.controller";

const auth = new AuthController();
const aritcles = new ArticleController();
const user = new UserController(); 
const author = new AuthorController();
const article_comment = new ArticleCommentController();

const userEmail = 'dimusha.mihail+1@gmail.com';
const userPassword = '956874lv';

describe('Tests for author path', () =>{
    let accessToken: string
    let userId;
    let articlesCounter: number;
    let aritcleId: string;
    let authorName = "";
    let authorSettings = {
        "avatar": null,
        "biography": "",
        "company": "Company",
        "firstName": "AuthorName",
        "id": userId,
        "job": "QA Software Engineer",
        "lastName": "LastLastName",
        "location": "Ukraine",
        "twitter": "https://twitter.com/",
        "userId": userId,
        "uploadImage": null,
        "website": ""
    }
    before('Try to get access token and userId', async ()=>{
        let response = await auth.authenticateUser(userEmail, userPassword);
        checkStatusCode(response, 200);
        checkResponseTime(response);
        accessToken = response.body.accessToken;

        response = await user.getCurrentUser(accessToken);
        checkStatusCode(response, 200);
        checkResponseTime(response);
        userId = response.body.id;
    });

    it('Get author settings', async() => {
        let response = await author.getAuthorSettings(accessToken);
        let authorName = response.body.firstName + response.body.lastName;
        checkStatusCode(response, 200);
        checkResponseTime(response);
    });

    it('Change author settings', async () => {
        let response = await author.setAuthorSettings(accessToken, authorSettings)
        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkResponseBodyMessage(response,"Success. Your profile has been updated.")
    });

    it('Get public author', async () =>{
        let response = await author.getPublicAuthor(userId);
        articlesCounter = response.body.length;
        checkStatusCode(response, 200);
        checkResponseTime(response);
    });

    it('Save article', async ()=>{
        let articleBody = {
            "authorId": userId,
            "authorName": authorName,
            "id": userId,
            "image": "https://www.industrialempathy.com/posts/image-optimizations/",
            "name": "TestArticle",
            "text": "Hi, I am  a new article"
        }
        let response = await aritcles.saveArticle(accessToken, articleBody);
        aritcleId = response.body.id;
        checkStatusCode(response, 200);
        checkResponseTime(response);

    });

    it('Get article info by articleId', async () =>{
        let response = await aritcles.getArticleById(aritcleId);
        checkStatusCode(response, 200);
        checkResponseTime(response);
    });

    it('Save new article comment', async () =>{
        let response = await article_comment.saveComment(accessToken, aritcleId, "New comment text");
        checkStatusCode(response, 200);
        checkResponseTime(response);
    });

    it('Get comments by article', async () =>{
        let response = await article_comment.getCommentsByArticle(aritcleId);
        checkStatusCode(response, 200);
        checkResponseTime(response);
    });

});