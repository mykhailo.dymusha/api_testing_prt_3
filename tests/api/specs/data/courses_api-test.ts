import {expect} from "chai";
import { describe } from "mocha";
import { CoursesController} from "../../lib/conrtollers/courses.controller";
import {checkResponseTime, checkStatusCode} from "../../../helpers/functionsForChecking.helper"
const courses = new CoursesController();


describe("Courses controller", () =>{
    it(`getAllCourseInfoById`, async () =>{
        let response = await courses.getAllCourseInfoById('1');
        checkStatusCode(response, 200);
        checkResponseTime(response);
    });

    it(`getPopularCourses`, async () =>{
        let response = await courses.getPopularCourses();
        checkStatusCode(response, 200);
        checkResponseTime(response);
    });
});